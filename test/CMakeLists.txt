tdaq_package()
tdaq_add_executable(test_histo
  test_histo.cpp  
  INCLUDE_DIRECTORIES ../include ../external/variant/include
  LINK_LIBRARIES  ROOT::Core
)
