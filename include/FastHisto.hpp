#include <vector>
#include <iostream>
#include <functional>
#include <unordered_map>
#include <memory>
#include <utility>
#include <typeinfo>
#include <type_traits>
#include <variant.hpp>

#ifndef __FAST_HISTO_HPP__
#define  __FAST_HISTO_HPP__

namespace FastHisto {
  template <typename T> class Histo2BaseImpl {
  public:
    Histo2BaseImpl() : _n(0),_overflow(0) {};
    virtual void fill(uint32_t  x,uint32_t y,T val=T(1))=0;
    virtual uint32_t operator () (uint32_t x,uint32_t y)=0;
    uint32_t entries() const {return _n;}
    uint32_t overflows() const {return _overflow;}
    virtual const std::string &name() const =0;
    virtual const std::string &x_title() const =0;
    virtual const std::string &y_title() const =0;
    virtual void toVariant(variant32 &var)=0;
  protected:
    uint32_t _n;
    uint32_t _overflow;
  };
  using Histo2Base=Histo2BaseImpl<uint32_t>;
  
  template <typename T,uint32_t x_max,uint32_t y_max> class Histo2IntImpl :
    public Histo2Base
  {
  public:    
    Histo2IntImpl(const std::string &name,
		  const std::string &x_title,
		  const std::string &y_title) : Histo2Base(),_name(name),_x_title(x_title),_y_title(y_title), _data((x_max)*(y_max)) {
      _p=_data.data();
    }
    inline void fill(uint32_t x,uint32_t y,uint32_t val=1) {
      _n++;
      if((x>=x_max) || (y>=y_max)) _overflow++;
      else
	_data[x+y*y_max]+=val;
    }
    inline uint32_t operator () (uint32_t x,uint32_t y) {
      return _data[x+y*y_max];
    }
    constexpr uint32_t xbins() {return x_max;}
    constexpr uint32_t ybins() {return y_max;}
    const std::string &name() const {return _name;};
    const std::string &x_title() const {return _x_title;};
    const std::string &y_title() const {return _y_title;};
    T &data() {return _data;}
    void toVariant(variant32 &var) {
      variant32 hist;
      hist["xbins"]=xbins();
      hist["ybins"]=ybins();
      hist["overflows"]=overflows();
      hist["entries"]=entries();
      hist["type"]=_type();
      hist["x_title"]=x_title();
      hist["y_title"]=y_title();
      hist["data"]=_data;
      hist["name"]=_name;
      var=hist;
    }

  private:
    const std::string _type() const  {
      std::string t;
      if constexpr (std::is_same<T,uint8_t>::value) t="u8";
      else if constexpr(std::is_same<T,uint16_t>::value) t="u16";
      else if constexpr(std::is_same<T,uint32_t>::value) t="u32";
      else static_assert(true,"Requires an integer type");
      return "h2d_"+t+"_"+std::to_string(x_max)+"_"+std::to_string(y_max);
    }

    std::vector<T> _data;
    T * _p;
    std::string _name;
    std::string _x_title;
    std::string _y_title;
  };
  using func=std::function<std::shared_ptr<Histo2Base>(
						       const std::string&,
						       const std::string&,
						       const std::string&
						       )>;
  class Histo2Int :public Histo2Base {
  public:
    Histo2Int()=delete;
    Histo2Int(Histo2Base &other)  {
      h=std::shared_ptr<Histo2Base>(&other,[](Histo2Base const*){}) ;
    } ;
    Histo2Int(const std::string &histo_type,
	      const std::string &name="",
	      const std::string &x_title="",
	      const std::string &y_title="") : Histo2Base()  {
      h=histo_map.at(histo_type)(name,x_title,y_title);
    }
    inline void fill(uint32_t x,uint32_t y,uint32_t val=1) {h->fill(x,y,val);};
    inline uint32_t operator () (uint32_t x,uint32_t y) { return h->operator()(x,y); }
    inline void toVariant(variant32 &v) {  h->toVariant(v); }
    //    const std::type_index type() const {return typeid(Histo2Int);}
    inline const std::string &name() const  {return h->name();}
    inline const std::string &x_title() const {return h->x_title();}
    inline const std::string &y_title() const {return h->y_title();}
  private:
    
  template <typename T,uint32_t x_max,uint32_t y_max>  static std::shared_ptr<Histo2Base>
  createInstance(const std::string& name,
		 const std::string& x_title,
		 const std::string& y_title) {
    return std::make_shared<Histo2IntImpl<T,x_max,y_max>>(name,x_title,y_title);
  }
    
    
    static const inline  std::map<const std::string,
				  const func> histo_map= {
      {"h2d_u8_100_100",createInstance<uint8_t,100,100>},
      {"h2d_u8_200_200",createInstance<uint8_t,200,200>},
    };
    std::shared_ptr<Histo2Base> h;
  };
} //namespace

#endif
